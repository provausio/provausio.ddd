using Provausio.DDD.EventSourcing.Aggregate;
using Xunit;

namespace Provausio.DDD.EventSourcing.Tests.Aggregate
{
    public class AggregateEventTests
    {
        [Fact]
        public void Create_GeneratesAnId()
        {
            // arrange
            
            // act
            var ev = AggregateEvent.Create<TestEvent1>();
            
            Assert.NotEmpty(ev.Id);
            Assert.NotNull(ev.Id);
        }
    }

    public class TestEvent1 : AggregateEvent
    {
        
    }
}