using System;
using System.Linq;
using System.Xml.Serialization;
using Provausio.DDD.EventSourcing.Aggregate;
using Xunit;

namespace Provausio.DDD.EventSourcing.Tests.Aggregate
{
    public class AggregateRootTests
    {
        [Fact]
        public void Raise_QueuesEvent()
        {
            // arrange
            
            // act
            var test = TestRoot.Create("foo", 1);

            // assert
            Assert.Equal(1, test.UncommittedEvents.Count);
            Assert.IsType<CreationEvent>(test.UncommittedEvents.First());
        }

        [Fact]
        public void Raise_VersionIsIncremented()
        {
            // arrange
            var test = TestRoot.Create("foo", 1);
            
            // act
            test.Action1();
            
            // assert
            Assert.Equal(2, test.Version);
        }

        [Fact]
        public void LoadFromStream_BringsObjectToState()
        {
            // arrange
            var id = Guid.NewGuid().ToString();
            var test = new TestRoot { Id = id };
            var create = new CreationEvent {Id = id, RootId = id, Version = 0, Prop1 = "foo", Prop2 = 1};
            var action1 = new Action1Occured {RootId = id, Version = 1};
            
            // act
            test.LoadFromStream(new AggregateEvent[] { create, action1 });
            
            // assert
            Assert.Equal(0, test.UncommittedEvents.Count);
            Assert.Equal(id, test.Id);
            Assert.True(test.Action1Occured);
            Assert.Equal(1, test.Version);
        }

        [Fact]
        public void CreateEntity_CreatesEntity()
        {
            // arrange
            var test = TestRoot.Create("foo", 1);
            
            // act
            test.CreateEntity("entity-foo");
            
            // assert
            Assert.Equal("entity-foo", test.EntityProp);
        }

        [Fact]
        public void UpdateEntity_ProxiesEventsToEntity()
        {
            // arrange
            var test = TestRoot.Create("foo", 1);
            test.CreateEntity("entity-foo");
            
            // act
            test.UpdateEntity("entity-bar");
            
            // assert
            Assert.Equal("entity-bar", test.EntityProp);
        }
    }
}