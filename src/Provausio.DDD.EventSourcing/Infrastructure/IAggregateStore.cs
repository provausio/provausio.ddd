using System.Threading.Tasks;
using Provausio.DDD.EventSourcing.Aggregate;

namespace Provausio.DDD.EventSourcing.Infrastructure
{
    public interface IAggregateStore<T>
        where T : AggregateRoot
    {
        /// <summary>
        /// Saves the aggregate root.
        /// </summary>
        /// <param name="root">The instance that will be saved.</param>
        /// <returns></returns>
        Task Save(T root);
        
        /// <summary>
        /// Retrieves an aggregate root.
        /// </summary>
        /// <param name="id">The ID of the aggregate.</param>
        /// <returns></returns>
        Task<T> GetById(string id);
    }
}