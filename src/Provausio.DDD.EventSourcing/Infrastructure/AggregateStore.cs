namespace Provausio.DDD.EventSourcing.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Aggregate;

    public abstract class AggregateStore<T> : IAggregateStore<T>
        where T : AggregateRoot
    {
        public async Task Save(T root)
        {
            Console.Out.WriteLine($"Simulated save aggregate {root.Id}");
            await Save(root.UncommittedEvents).ConfigureAwait(false);
            root.ClearEvents();
        }

        public async Task<T> GetById(string id)
        {
            var instance = Activator.CreateInstance<T>();
            instance.SetRootId(id);
            await GetById(id, stream => instance.LoadFromStream(stream));
            Console.Out.WriteLine($"Simulated lookup aggregate {id}");
            return instance;
        }

        protected abstract Task GetById(string id, Action<IEnumerable<IAggregateEvent>> callback);
        protected abstract Task Save(IEnumerable<IAggregateEvent> stream);
    }
}