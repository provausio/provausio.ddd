using System;
using XidNet;

namespace Provausio.DDD.EventSourcing.Aggregate
{
    public class AggregateEvent : IAggregateEvent
    {
        public string Id { get; set; }

        public string RootId { get; set; }

        public long Version { get; set; }

        public static T Create<T>()
            where T : IAggregateEvent, new()
        {
            var instance = Activator.CreateInstance<T>();
            instance.Id = Xid.NewXid().ToString();
            return instance;
        }
    }
}