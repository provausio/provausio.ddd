### Updating State
Updating state happens in 3 stages: Action, Raise, and Apply

#### Action
Action is the semantically named method that the caller will interact with. This should be named in domain terms such as `PlaceOrder(...)` or `DeleteItem(...)`

The body of the action should *not* update the state of the object. This stage is intended as an opportunity for the Aggregate Root to reject the command. In other words, this is where you're going to validate the input values as well as validate the action against the current state of the model to determine whether or not the Aggregate Root "okays" the command. Only if the validation succeeded should the event be raised. The exception to this rule is an error event being raised. This is a developer choice and depends on your domain strategy.

#### Raise
Should the previous stage succeed, then an event signifying that the action took place should be raised, indicating that the state of the object should be updated.

Example:

``` csharp
public void MyAction(string myParameter)
{
    // validate the input
    if(string.IsNullOrempty(myParameter))
        throw new ArgumentNullException(nameof(myParameter));
        
    // if we should succeed, raise the event
    Raise<ActionOccurred>(c => 
    {
        c.ActionDate = DateTimeOffset.UtcNow();
    });
}
```

#### Apply
Each event that can occur on the model should have a corresponding `Apply(T evnt)` method to handle the state mutation. So for our event that we described above, it might look like this:

``` csharp
private void Apply(ActionOccured evnt)
{
    this._lastAcction = evnt.ActionDate;
}
```

The apply event needs to assume that the incoming event is valid because in order for the event to ever be raised, the data must be validated. The reason that we separate the two steps is for Replay which we'll cover here in a second.

In order for the Aggregate Root to know which Apply methods to send events, handlers must be registered. Do this in the required override `RegisterHandlers` like so:

``` csharp
protected override RegisterHandlers()
{
    // type inferrence will resolve this to this.Apply(ActionOccurred)
    RegisterHandler<ActionOccurred>(Apply);    
}
```

### Entities (non-root)
Entities which are not a root are a way to separate our business logic into constructs that are recognized by the business domain. There are a few rules from *Domain Driven Design* which are enforced here:
- Only Aggregate Roots (AR) should be saved to our persistence layer
- Both ARs and non-root Entities should have unique identifiers. Roots should have IDs that are globally unique, and Entities should have IDs that are at least unique within the aggregate.
- Entities should never be accessed directly. They should only be used *through* the AR.

To maintain consistency while also trying to keep our patterns within each model type consistent, Entities can deal with events on their own, but through the root. For example:

``` csharp
internal sealed class MyNonRootEntity : Entity
{
    private DateTimeOffset _lastActionDate;

    public void SomeAction()
    {
        Raise<MyNonRootAction>(c => 
        {
            c.ActionDate = DateTimeOffset.UtcNow();
        });
    }

    private void Apply(MyNonRootAction evnt)
    {
        _lastActionDate = evnt.ActionDate;
    }

    protected override void RegisterHandlers(AggregateRoot root)
    {
        root.RegisterHandler<MyNonRootAction>(Apply);
    } 
}
```

As you can see, the pattern is very similar. The only real difference here is that you register handlers on the root instead of on the base class. This ensures that events are written to the root's stream instead of some other stream.

The root is added to the Entity on creation, if you use the provided convenience method on the AR to spawn the entity. Doing this will ensure IDs are correctly generated and that the entity is corrently bound to its AR. Doing this is easy; simply call `CreateEntity<T>` on the AR.

``` csharp
public sealed class MyRoot : AggregateRoot
{
    private MyNonRootEntity _myEntity;
    
    public void CreateEntity()
    {
        Raise<EntityCreated>(c => 
        {
            c.CreationDate = DateTimeOffset.UtcNow();
        });
    }
    
    public void DoActionOnEntity()
    {
        _myEntity.SomeAction();
    }
    
    private void Apply(EntityCreated evnt)
    {
        _myEntity = CreateEntity<MyNonRootEntity>();
        _myEntity.CreationDate = evnt.CreationDate;
    }
}
```

Ok so I kind of pulled a fast one here. Notice that there are some extra steps here: 

First, there is a method that creates the entity. It follows the normal pattern where it raises the event and then on the Apply, the actual state change happens. In this case, it calls the `CreateEntity` method and then mutates its state.

Second, there is a method that mutates the entity. In this step, the AR calls the entity directly and delegates the action to that entity. That entity will, in turn, behave in a similar pattern where it raises and applies the event.

Why didn't we just delegate the creation of the entity to the entity itself? Well if you think about it for a bit, you'll quickly realize that you're going to run in to a chicken or the egg type situation. If the handling of the creation event was delegated to the non-root entity, but the non-root entity doesn't yet exist, then the event cannot be applied. For this reason, we let the AR handle the creation event, but everything else that is actually performed by the non-root entity will be handled by that entity.

An easy way of remember this pattern is to simply recall that the **entity that performed the action should be responsible for applying it.** This makes sense in context: it was the AR that created the entity, but the update of the entity actually took place in the entity itself. All the AR did in that scenario was hand off the command given by the caller.

Also notice that I've been sealing classes. This is more of a practice thing. In the domain, I don't think you should be inheriting models. I also mark entities as *internal*. I do this because entities shouldn't really be used outside of the domain. This just makes it harder to leak internals, because the compiler won't let you. I always take this further by making sure my domain is modeled in its own assembly. The application/service layer will then only be able to interact with public objects such as the AR, which makes it a bit easier to enforce the model.