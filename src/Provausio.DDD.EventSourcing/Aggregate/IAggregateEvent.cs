namespace Provausio.DDD.EventSourcing.Aggregate
{
    public interface IAggregateEvent
    {
        string Id { get; set; }
        
        string RootId { get; set; }

        long Version { get; set; }
    }
}