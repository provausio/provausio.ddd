using System;
using Provausio.DDD.EventSourcing.Aggregate;
using Provausio.DDD.EventSourcing.Sample.Accounts.Ledgers;

namespace Provausio.DDD.EventSourcing.Sample.Accounts
{
    public class Account : AggregateRoot
    {
        private Ledger _ledger;

        public Money Balance => _ledger.Balance;
        
        public static Account Create(string owner, Money openingBalance, decimal overdraftAllowance)
        {
            var account = new Account();
            account.Raise<AccountCreated>(c =>
            {
                c.Owner = owner;
                c.OpeningBalance = openingBalance.Amount;
                c.CurrencyType = openingBalance.Type;
                c.CreationDate = DateTimeOffset.UtcNow;
                c.LedgerId = Guid.NewGuid().ToString();
                c.OverdraftAllowance = overdraftAllowance;
                c.AccountId = Guid.NewGuid().ToString();
            });

            return account;
        }

        public void Deposit(Money amount)
        {
            _ledger.Deposit(amount);
            Console.Out.WriteLineAsync($"Deposited {amount}. New Balance: {_ledger.Balance}");
        }

        public void Withdraw(Money amount)
        {
            _ledger.Withdraw(amount);
            Console.Out.WriteLineAsync($"Withdrew {amount}. New Balance: {_ledger.Balance}");
        }

        private void Apply(AccountCreated e)
        {
            SetRootId(e.AccountId);
            _ledger = CreateEntity<Ledger>(idFactory => e.LedgerId);
            var ledgerState = new Ledger.LedgerState
            {
                Balance = new Money(e.OpeningBalance, e.CurrencyType),
                LedgerId = _ledger.Id,
                OverdraftAllowance = e.OverdraftAllowance
            };
            
            _ledger.RestoreState(ledgerState);
        }
        
        protected override void RegisterHandlers()
        {
            RegisterHandler<AccountCreated>(Apply);
        }
    }

    public class AccountCreated : AggregateEvent
    {
        public string AccountId { get; set; }
        public string Owner { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public decimal OpeningBalance { get; set; }
        public string CurrencyType { get; set; }
        public string LedgerId { get; set; }
        public decimal OverdraftAllowance { get; set; }
    }
}