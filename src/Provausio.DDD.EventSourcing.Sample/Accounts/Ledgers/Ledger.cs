using System;
using Provausio.DDD.EventSourcing.Aggregate;

namespace Provausio.DDD.EventSourcing.Sample.Accounts.Ledgers
{
    public class Ledger : Entity
    {
        private string _currencyType;
        private decimal _overdraftAllowance = 0;
        
        public Money Balance { get; private set; }

        
        public void Deposit(Money amount)
        {
            Raise<MoneyDeposited>(c =>
            {
                c.Amount = amount.Amount;
                c.Type = amount.Type;
            });
        }

        public void Withdraw(Money amount)
        {
            ValidateCurrency(amount);
            if(Balance.Subtract(amount).Amount < _overdraftAllowance)
                throw new InsufficientFundsException(Balance, amount, $"Withdrawal would exceed allowed overdraft allowance of {_overdraftAllowance:C}.");
            
            Raise<MoneyWithdrawn>(c =>
            {
                c.Amount = amount.Amount;
                c.Type = amount.Type;
            });
        }

        private void ValidateCurrency(Money amount)
        {
            if(amount.Type != _currencyType)
                throw new InvalidOperationException($"Ledger is not configured to handle currency '{amount.Type}'");
        }
        
        private void Apply(MoneyDeposited e)
        {
            var amount = new Money(e.Amount, e.Type);
            Balance = Balance.Add(amount);
        }

        private void Apply(MoneyWithdrawn e)
        {
            var amount = new Money(e.Amount, e.Type);
            Balance = Balance.Subtract(amount);
        }

        public void RestoreState(LedgerState state)
        {
            Id = state.LedgerId;
            Balance = state.Balance;
            _currencyType = state.Balance.Type;
            _overdraftAllowance = state.OverdraftAllowance;
        }
        
        public override void RegisterHandlers(AggregateRoot root)
        {
            root.RegisterHandler<MoneyDeposited>(Apply);
            root.RegisterHandler<MoneyWithdrawn>(Apply);
        }

        public class LedgerState
        {
            public Money Balance { get; set; }
            public decimal OverdraftAllowance { get; set; }
            public string LedgerId { get; set; }
        }
    }

    public class MoneyWithdrawn : AggregateEvent
    {
        public DateTimeOffset WithdrawalDate { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
    }

    public class MoneyDeposited : AggregateEvent
    {
        public DateTimeOffset DepositDate { get; set; }

        public decimal Amount { get; set; }

        public string Type { get; set; }
    }

    public class LedgerCreated : AggregateEvent
    {
        public string LedgerId { get; set; }
        public decimal OpeningBalance { get; set; }
        public string CurrencyType { get; set; }
        public decimal OverdraftAllowance { get; set; }
    }

    public struct Money
    {
        public readonly string Type;
        public readonly decimal Amount;
        
        public Money(decimal amount, string type)
        {
            Type = type;
            Amount = amount;
        }

        public Money Add(Money m)
        {
            ValidateType(m);
            return new Money(Amount + m.Amount, m.Type);
        }

        public Money Subtract(Money m)
        {
            ValidateType(m);
            return new Money(Amount - m.Amount, m.Type);
        }

        private void ValidateType(Money m)
        {
            if(m.Type != Type)
                throw new InvalidOperationException($"Cannot add two different types of money ({Type} and {m.Type}). Convert first, then try again.");
        }

        public override string ToString()
        {
            return Amount.ToString("C");
        }
    }

    public static class CurrencyType
    {
        public const string USD = "USD";
    }

    public class InsufficientFundsException : Exception
    {
        public Money Balance { get; }
        public Money WithdrawRequest { get; }

        public InsufficientFundsException(Money balance, Money withdrawRequest, string message)
            : base(message)
        {
            Balance = balance;
            WithdrawRequest = withdrawRequest;
        }
    }
}