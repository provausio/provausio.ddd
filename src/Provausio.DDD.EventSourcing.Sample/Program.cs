﻿using System;
using Provausio.DDD.EventSourcing.Sample.Accounts;
using Provausio.DDD.EventSourcing.Sample.Accounts.Ledgers;

namespace Provausio.DDD.EventSourcing.Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            var account = Account.Create("Jeremy Stafford", new Money(30000, CurrencyType.USD), 0);
            account.Withdraw(new Money(20000, CurrencyType.USD));
            account.Deposit(new Money(500, CurrencyType.USD));


            try
            {
                account.Withdraw(new Money(10501, CurrencyType.USD));
            }
            catch (InsufficientFundsException e)
            {
                Console.WriteLine($"{e.Message} -- Current Balance: {account.Balance}");
            }


            Console.ReadKey();
        }
    }
}